package com.example.sesi3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.sesi3.databinding.ActivityMainBinding
import java.lang.IllegalStateException

class MainActivity : AppCompatActivity() {

    private var _layout: ActivityMainBinding? = null

    private val layout: ActivityMainBinding
        get() = _layout ?: throw IllegalStateException("The activity has been destroyed")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        _layout = binding
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().apply {
            replace(layout.frameMain.id, MainFragment())
        }.commit()
    }
    override fun onDestroy() {
        super.onDestroy()
        _layout = null
    }
}