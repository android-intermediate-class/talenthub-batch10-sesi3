package com.example.sesi3

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sesi3.data.News
import com.example.sesi3.databinding.ItemNewsBinding
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.Locale

class NewsAdapter(
    private val mNews: ArrayList<News>,
    private val itemClick: (News) -> Unit
) : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val binding = ItemNewsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return NewsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(mNews[position])
    }

    override fun getItemCount(): Int = mNews.size

    inner class NewsViewHolder(
        private val binding: ItemNewsBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SimpleDateFormat")
        fun bind(news: News) {
            binding.root.setOnClickListener { itemClick(news) }
            val inputDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            val outputDateFormat = SimpleDateFormat("dd MMM yyyy", Locale("id", "ID"))
            val date = inputDateFormat.parse(news.publishedDate)

            Picasso.get()
                .load(news.imageUrl)
                .error(R.drawable.broken_image)
                .into(binding.ivItemNews)
            binding.tvItemNewsTitle.text = news.title
            binding.tvItemNewsDate.text = outputDateFormat.format(date)
        }
    }
}